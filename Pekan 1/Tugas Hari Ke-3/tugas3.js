//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var gabung = kataPertama + " " +(kataKedua[0].toUpperCase() + kataKedua.slice(1) + 
			" "+kataKetiga +" "+ kataKeempat.toUpperCase());


console.log("Jawaban soal 1 :");
console.log(gabung);


//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var hasil = Number(kataPertama) + Number(kataKedua) + 
Number(kataKetiga) + Number(kataKeempat);

console.log("Jawaban soal 2 :");
console.log(hasil); 


//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18);  
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Jawaban soal 3 :");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


//soal 4
console.log("Jawaban soal 4 :");
var nilai = 90;

if ( nilai > 80) {
	console.log("Anda mendapat nilai = A")
} else if((nilai > 70) && (nilai <= 80)) {
	console.log("Anda mendapat nilai = B")
} else if ((nilai > 60) && (nilai <= 70)) {
	console.log("Anda mendapat nilai = C")
}else if ((nilai > 50) && (nilai <= 60)) {
	console.log("Anda mendapat nilai = D")
}else if (nilai < 50) {
	console.log("Anda mendapat nilai = E")
}


//soal 5
var tanggal = 22;
var bulan = 7;
var tahun = 2020;
var namabulan = "";

 switch(bulan){
 	case 1 : namabulan = "Januari"; break;
 	case 2 : namabulan = "Februari"; break;
 	case 3 : namabulan = "Maret"; break;
 	case 4 : namabulan = "April"; break;
 	case 5 : namabulan = "Mei"; break;
 	case 6 : namabulan = "Juni"; break;
 	case 7 : namabulan = "Juli"; break;
 	case 8 : namabulan = "Agustus"; break;
 	case 9 : namabulan = "September"; break;
 	case 10 : namabulan = "Oktober"; break;
 	case 11 : namabulan = "November"; break;
 	case 12 : namabulan = "Desember";break;
 	default : namabulan = "Infalid"
 }
 var gabung = tanggal+ " " + namabulan + " " + tahun 
console.log("Jawaban soal 5 :");
console.log(gabung);