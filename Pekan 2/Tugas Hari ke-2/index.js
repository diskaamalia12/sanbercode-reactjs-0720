// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
 readBooks(1000, books[0], function(time){
 		readBooks(time, books[1], function(time){
 				readBooks(time, books[2], function(time){})
 		})
 })
// Tulis code untuk memanggil function readBooks di sini