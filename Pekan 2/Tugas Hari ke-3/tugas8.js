//soal 1
console.log("Jawaban soal 1 :");
let luas = (r) => {
    return 3.14 *  r * r 
} 
    console.log("Luas lingkaran = "+ luas(7))
    
let keliling = (r) => {
  return 2 * 3.14 * r
}
    console.log("Keliling lingkaran = "+ keliling(15))
    console.log(" ")

//soal 2
console.log("Jawaban soal 2 :");

let kalimat = ""

let kata1 = 'saya'
let kata2 = 'adalah'
let kata3 = 'seorang'
let kata4 = 'frontend'
let kata5 = 'developer'

kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`

console.log(kalimat)
console.log(" ")

//soal 3
console.log("Jawaban soal 3 :");
class Book{
  constructor(name, totalPage, price) {
    this.name = name 
    this.totalPage = totalPage
    this.price = price
  }
}
class Komik extends Book{
  constructor(name, totalPage, price, isColorful){
    super(name, totalPage, price, isColorful)
    this.isColorful = isColorful
  }
}
let buku = new Book("President", 500, 300000)
let manga = new Komik ("One Piece", 300, 200000, false)
 
console.log(buku) 
console.log(" ")
console.log(manga) 
