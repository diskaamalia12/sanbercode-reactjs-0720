//soal 1
console.log("Jawaban soal 1 :");

var daftarPeserta = {
  nama : "Asep",
  jeniskelamin : "laki-laki",
  hobi : "baca buku",
  tahunlahir : 1992,
} 
console.log(daftarPeserta)
console.log(" ")

//soal 2
console.log("Jawaban soal 2 :");

var buah1 = {
    nama : "strawberry",
    warna : "merah",
    adaBijinya : "tidak",
    harga : 9000 ,
}
var buah2 = {
    nama : "jeruk",
    warna : "oranye",
    adaBijinya : "ada",
    harga : 8000 ,
}
var buah3 = {
    nama : "Semangka",
    warna : "Hijau & Merah",
    adaBijinya : "ada",
    harga : 10000,
}
var buah4 = {
    nama : "Pisang",
    warna : "Kuning",
    adaBijinya : "tidak",
    harga : 5000, 
}

console.log(buah1)
console.log(" ")

//soal 3
console.log("Jawaban soal 3 :");

var dataFilm = [{nama : "Trolls", durasi : "120 menit", genre : "Cartoon", tahun : 2017}]

console.log(dataFilm)
console.log(" ")

//soal 4
console.log("Jawaban soal 4 :");

//release 0
console.log("release 0")
class Animal {
    constructor(name, legs, cold_blooded) {
        this.name = name
        this.legs = 4
        this.cold_blooded = "false"
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 
console.log(" ")

//release 1
console.log("release 1")
class Ape extends Animal{
  constructor(name){
      super(name)
      this.legs = 2
}
  yell(){
    console.log("Auoo")
  }
  }
class Frog extends Animal{
  constructor(name){
      super(name)
      this.legs = 2
}
  jump(){
    console.log("hop hop")
  }
 }
var sungokong = new Ape("kera sakti")
sungokong.yell() 
 
var kodok = new Frog("buduk")
kodok.jump()  
console.log(" ")

//soal 5
console.log("Jawaban soal 5 :");

class Clock {

  constructor({ template }) {
    this.template = template;
  }
  
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();





